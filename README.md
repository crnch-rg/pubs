# CRNCH Rogues Gallery Publications

This repository contains publically available presentations related to the CRNCH Rogues Gallery hosted by Georgia Tech. For any questions please email us at [this address](crnch-rg-admin@lists.gatech.edu).

## 2019
* Jeffrey S. Young, Jason Riedy, Thomas M. Conte, Vivek Sarkar, Prasanth Chatarasi, Sriseshan Srikanth, Experimental Insights from the Rogues Gallery, International Conference on Rebooting Computing, November 2019. [Paper](https://gitlab.com/crnch-rg/pubs/blob/master/2019/ICRC_2019/young_et_al_exp_insights_rogues_gallery_icrc_2019.pdf) [Presentation](https://gitlab.com/crnch-rg/pubs/blob/master/2019/ICRC_2019/young_et_al_exp_insights_rogues_gallery_icrc_2019_slides.pdf)
* Will Powell, Jason Riedy, Jeffrey S. Young, and Thomas M. Conte. 2019. Wrangling Rogues: A Case Study on Managing Experimental Post-Moore Architectures. In Proceedings of the Practice and Experience in Advanced Research Computing on Rise of the Machines (learning) (PEARC '19). [Paper]() [Presentation](https://gitlab.com/crnch-rg/pubs/blob/master/2019/PEARC_2019/powell_riedy_et_all_wrangling_rogues_pearc19_slides.pdf)  
